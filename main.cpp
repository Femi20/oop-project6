#include "student.h"
#include <fstream>
#include <cstring>
#include <string>
#include <iomanip>
using namespace std;


int main()
{
	
	char fileName[100];
	ifstream in;
	ofstream out;
	int size;
	
	do
	{
		in.clear();
		
		cout << "Please enter the name of the input file.\n";
		cout << "Filename: ";
		cin >> fileName;
		
		in.open(fileName);
		
		if(!in)
			cout << "That is not a valid file. Please try again.\n";
		
		
	}while(!in);
	
	do
	{
		out.clear();
		
		cout << "Please enter the name of the output file.\n";
		cout << "Filename: ";
		cin >> fileName;
		
		out.open(fileName);
		
		if(!out)
			cout << "That is not a valid file. Please try again.\n";
		
		
	}while(!in);
	
	cout << "Processing Complete" << endl;
	
	in >> size;
	in.ignore(1,'\n');
	Student* roster[size];
	
	
	for(int i = 0; i < size; i++)
	{
		string course, first, last;
		getline(in, last, ',');
		in.ignore(1,'\n');
		getline(in, first);
		getline(in, course, ' ');
		in.ignore(0,'\n');

		
		if(course.compare("English") == 0)
		{
			double term, mid, finalExam;
		
			in >> term >> mid >> finalExam;	
			
			in.ignore(1,'\n');
			
			roster[i] = new EngStudent(first, last, course, term, mid, finalExam);
		}
	
	
		if(course.compare("History") == 0)
		{
			double att, proj, mid, finalExam;
			
			in >> att >> proj >> mid >> finalExam;
			in.ignore(1,'\n');
					
			roster[i] = new HistStudent(first, last, course, att, proj, mid, finalExam);
		}
	
	
	
		if(course.compare("Math") == 0)
		{
			double q1, q2, q3, q4, q5, t1, t2, finalEx;
			
			in >> q1 >> q2 >> q3 >> q4 >> q5 >> t1 >> t2 >> finalEx;
			in.ignore(1,'\n');
		
			roster[i] = new mathStudent(first, last, course, q1, q2, q3, q4, q5, t1, t2, finalEx);
	
		}
		
	}
		
	for(int i = 0; i < size; i++)
	{
		roster[i] -> calcAvg();
	}
	
	out << "Student Grade Summary\n";
	out << "---------------------\n\n";
	
	out << "ENGLISH CLASS\n\n";
	out << setw(42) << left << "Student" << setw(8) << "Final" << setw(8) << "Final" << setw(8)	<< "Letter \n";
	out << setw(42) << left << "Name" << setw(8) << "Exam" << setw(8) <<  "Avg"<< setw(8) << "Grade \n";
	out << "---------------------------------------------------------------- \n";
	
	for(int i = 0; i < size; i++)
	{
		if(roster[i] -> getCourse().compare("English") == 0)
		{
			out <<  setw(42) << left << roster[i]->getFirst() + " " +  roster[i]->getLast();
			out << left << setw(8) << roster[i] -> getFinalGrade();
			out << fixed << setprecision(2) << setw(8) << left << roster[i]->getFinalAvg();
			out << left << setw(3) <<  roster[i]->getLetter(roster[i]->getFinalAvg()) << endl;
			
		}
	}
	
	out << "\n\n";
	
	out << "HISTORY CLASS\n\n";
	out << setw(42) << left << "Student" << setw(8) << "Final" << setw(8) << "Final" << setw(8)	<< "Letter \n";
	out << setw(42) << left << "Name" << setw(8) << "Exam" << setw(8) <<  "Avg"<< setw(8) << "Grade \n";
	out << "----------------------------------------------------------------" << endl;
	
	for(int i = 0; i < size; i++)
	{
		if(roster[i] -> getCourse().compare("History") == 0)
		{
			out <<  setw(42) << left << roster[i]->getFirst() + " " +  roster[i]->getLast();
			out << left << setw(8) << roster[i] -> getFinalGrade();
			out << fixed << setprecision(2) << setw(8) << left << roster[i]->getFinalAvg();
			out << left << setw(3) <<  roster[i]->getLetter(roster[i]->getFinalAvg()) << endl;
			
		}
	}
	
	out << "\n\n";
	
	out << "MATH CLASS\n\n";
	out << setw(42) << left << "Student" << setw(8) << "Final" << setw(8) << "Final" << setw(8)	<< "Letter \n";
	out << setw(42) << left << "Name" << setw(8) << "Exam" << setw(8) <<  "Avg"<< setw(8) << "Grade \n";
	out << "----------------------------------------------------------------   \n";
	
	for(int i = 0; i < size; i++)
	{
		if(roster[i] -> getCourse().compare("Math") == 0)
		{
			out <<  setw(42) << left << roster[i]->getFirst() + " " +  roster[i]->getLast();
			out << left << setw(8) << roster[i] -> getFinalGrade();
			out << fixed << setprecision(2) << setw(8) << left << roster[i]->getFinalAvg();
			out << left << setw(3) <<  roster[i]->getLetter(roster[i]->getFinalAvg()) << endl;
			
		}
	}
	

	out << "\n\n";
	
	out << "OVERALL GRADE DISTRIBUTION\n\n";
	
	int a = 0;	int b = 0;	int c = 0;	int d = 0;	int f = 0;
		
	for(int i=0; i<size; i++)
	{
		char letter = roster[i]->getLetter(roster[i]->getFinalAvg());
	
		switch(letter)
		{
			case 'A': a++; break;
			
			case 'B': b++; break;
			
			case 'C': c++; break;
			
			case 'D': d++; break;
			
			case 'F': f++; break;
				
		}
		
	}
	
	out << "A:   " << a << endl;
	out << "B:   " << b << endl;
	out << "C:   " << c << endl;
	out << "D:   " << d << endl;
	out << "F:   " << f << endl;
	
	
	out.close();
	in.close();
	
	
	for(int i = 0; i < size; i++)
	{
		delete roster[i];	
	}
	
	
	return 0;
	
}