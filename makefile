student: main.o student.o
	g++ -o student main.o student.o
	
main.o: student.h main.cpp
	g++ -c main.cpp
	
student.o: student.h student.cpp
	g++ -c student.cpp
	
clean:
	rm *.o student
