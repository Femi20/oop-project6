#include <iostream>
#include <string>
#include <iomanip>
#include <cctype>
#include "student.h"



using namespace std;

Student::Student(string f, string l, string c, double exam)
{
	firstName = f;
	lastName = l;
	courseName = c;
	finalExam = exam;
	finalAvg = 0;
}

char Student::getLetter (double avg)
{
	int a = avg/10;
	
	switch(a)
	{
		case 10:
		case 9:
			return 'A';
			break;
		
		case 8:
			return 'B';
			break;
			
		case 7:
			return 'C';
			break;
		
		case 6:
			return 'D';
			break;
			
		default:
			return 'F';
			
	}
	
}

//Accessor Methods
double Student::getFinalAvg()
{
	return finalAvg;
}

string Student::getLast() const
{
	return lastName;
}

string Student::getFirst() const
{
	return firstName;
}

string Student::getCourse() const
{
	return courseName;
}
	
int Student::getFinalGrade() const
{
	return finalExam;
}
	

	
EngStudent:: EngStudent(string f, string l, string c, double paper, double mid, double exam): Student(f, l, c, exam)
{
	termPaper = paper;
	midTerm = mid;
	
}

void EngStudent::calcAvg()
{
	finalAvg = ((0.25*termPaper)+(0.35*midTerm)+(0.40*finalExam));
}



		
HistStudent::HistStudent(string f, string l, string c, double att, double proj, double mid, double exam):
			Student(f,l,c,exam)
{
	
	attendance = att;
	project = proj;
	midTerm = mid;
	
}
	
void HistStudent::calcAvg()
{
	finalAvg = (0.10*attendance )+ (0.30 * project)+(0.30*midTerm)+(0.30 * finalExam);
}



mathStudent::mathStudent(string f, string l, string c, double q1, double q2, double q3, double q4, double q5, double t1, double t2, double exam):
			Student(f,l,c,exam)
{
	quiz1 = q1;
	quiz2 = q2;
	quiz3 = q3;
	quiz4 = q4;
	quiz5 = q5;
	test1 = t1;
	test2 = t2;
	
}
					
void mathStudent::calcAvg()
{
	double quizAvg = (quiz1+quiz2+quiz3+quiz4+quiz5)/5.0;
	
	finalAvg = (0.15*quizAvg) + (0.25*test1) + (0.25*test2) + (0.35*finalExam);
	
}
	

	
		
