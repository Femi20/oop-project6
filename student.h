#include <iostream>
#include <string>

using namespace std;


#ifndef _STUDENT_H_
#define _STUDENT_H_

class Student
{
protected:
	string firstName;
	string lastName;
	string courseName;
	double finalAvg;
	double finalExam;
	Student(string f, string l, string c, double exam);
	

public:	
	virtual void calcAvg() = 0;
	char getLetter (double avg);
	double getFinalAvg();
	string getLast() const;
	string getFirst() const;
	string getCourse() const;
	int getFinalGrade() const;
	
};



class EngStudent: public Student
{
	public:
		EngStudent(string f, string l, string c, double paper, double mid, double exam);
		void calcAvg();
	
	private:
		double termPaper, midTerm;
};


class HistStudent: public Student
{
	public:
		HistStudent(string f, string l, string c, double att, double proj, double mid, double exam);
		void calcAvg();
		
	private:
		double attendance, project, midTerm;
};

class mathStudent: public Student
{
	public:
		mathStudent(string f, string l, string c, double q1, double q2, double q3, double q4, double q5, double t1, double t2, double exam);
		void calcAvg();
	
	private:
			double quiz1, quiz2, quiz3, quiz4, quiz5, test1, test2;
		
};

#endif
	
		